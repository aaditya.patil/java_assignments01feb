package javaassignmentsQuestionFirst;

public class Santro extends Car implements BasicCar{
	
	public void remoteStart()
	{
		System.out.println("remotestart a car");
	}

	public void gearChange() {
		
		System.out.println("gearchange a car");
		
	}

	public void music() {
		
		System.out.println("music a car");
		
	}
	
	public void drive()
	{
		System.out.println("drive a car override child class");
	}
	
	public void stop()
	{
		System.out.println("car stop override child class");
	}
	
	public static void main(String[] args) {
		
		Santro s = new Santro();
		s.remoteStart();
		s.gearChange();
		s.music();
		s.drive();
		s.stop();
	}


}
