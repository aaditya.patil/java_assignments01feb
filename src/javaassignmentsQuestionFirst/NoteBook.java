package javaassignmentsQuestionFirst;

public class NoteBook extends Book {

	@Override
	public void write() {
			
		System.out.println("Write a book");
	}

	@Override
	public void read() {
		
		System.out.println("read a book");
	
	}
	
	public void draw()
	{
		System.out.println("draw a book");
	}
	
	public static void main(String[] args) {
		
		NoteBook n = new NoteBook();
		n.write();
		n.read();
		n.draw();
	}

}
