package javaassignmentsQuestionFirst;


public class Orange {

	public static void main(String[] args) {

		String str = "This is orange juice";

		if (str.contains("orange"))
		{
			System.out.println("The string contains the word 'orange'");
			
		} 
		else
		{
			System.out.println("The string does not contain the word 'orange'");
		}

	}

}
