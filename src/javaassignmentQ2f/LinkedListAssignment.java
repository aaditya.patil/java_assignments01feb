package javaassignmentQ2f;

import java.util.LinkedList;

public class LinkedListAssignment {

	public static void main(String[] args) {
		
		LinkedList<String> list= new LinkedList<String>();
		
	    list.add("orange");
	    list.add("pink");
	    list.add("green");
	    list.add("Coke");
	    list.add("blue");
	        
	    System.out.println("size of the linked list " + list.size());
	    System.out.println("Total List " + list);
	    
        list.remove(1);
	    
	    System.out.println("Remove the Linkedlist is " + list);
	        
	    boolean b =list.contains("Coke");
	    System.out.println("Coke is present: " + b);
	}
	    

	}


