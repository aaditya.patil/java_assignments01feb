package javaassignmentQ2f;

import java.util.ArrayList;

public class ArrayListAssignment {
	
	public static void main(String[] args) {
		
		ArrayList<String> list= new ArrayList<String>();
		
	    list.add("orange");
	    list.add("pink");
	    list.add("green");
	    list.add("Coke");
	    list.add("blue");
	        
	    System.out.println("size of the arraylist " +list.size());
	    System.out.println("Total list "+ list);
	    
	    list.remove(1);
	    
	    System.out.println("Remove the arraylist is " +list);
	        
	    boolean b =list.contains("Coke");
	    System.out.println("Coke is present: " + b);
	}
	

}
