package javaassignmentQuestionSecond;

public class TV implements TVRemote {
	
	public void remote()
	{
		System.out.println("remote method");
	}

	@Override
	public void smartRemotes() {
		
		System.out.println("smartRemote method");
		
	}
	
	public static void main(String[] args) {
		
		TV t = new TV();
		t.remote();
		t.smartRemotes();
	}

}
