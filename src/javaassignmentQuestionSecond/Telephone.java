package javaassignmentQuestionSecond;

public abstract class Telephone {
	
	public abstract void with();
	public abstract void lift();
	public abstract void disconnected();

}
