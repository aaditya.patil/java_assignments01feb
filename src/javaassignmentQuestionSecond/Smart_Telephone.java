package javaassignmentQuestionSecond;

public class Smart_Telephone extends Telephone{

	@Override
	public void with() {
		
		System.out.println("with method");
		
	}

	@Override
	public void lift() {
		
		System.out.println("lift method");
	}

	@Override
	public void disconnected() {
		
		System.out.println("disconnected method");
	}
	
	public static void main(String[] args) {
		
		Smart_Telephone s = new Smart_Telephone();
		s.with();
		s.lift();
		s.disconnected();
		
	}

}
