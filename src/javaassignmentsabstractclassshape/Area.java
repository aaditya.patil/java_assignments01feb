package javaassignmentsabstractclassshape;

public class Area extends Shape {
	
	@Override
	public void RectangleArea(int length, int breadth)
	{
		int rectArea=length * breadth;
		System.out.println("Area of Rectangle: " +rectArea);
    }

	@Override
	public void SquareArea(int side) {

		int squArea=side * side;
		System.out.println("Area of Square: " +squArea);
    }

	@Override
	public void CircleArea(double radius)
	{

		double circleArea=3.14 * radius *radius;
	    System.out.println("Area of circle: " +circleArea);
    }

	public static void main(String[] args) 
	{
		Area a= new Area();
		a.RectangleArea(20, 30);
		a.SquareArea(20);
		a.CircleArea(40);
	}

}
