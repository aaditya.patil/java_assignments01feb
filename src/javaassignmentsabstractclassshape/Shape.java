package javaassignmentsabstractclassshape;

public abstract class Shape {
	
	public abstract void RectangleArea(int length, int breath);
	public abstract void SquareArea(int side);
	public abstract void CircleArea(double radius);

}
